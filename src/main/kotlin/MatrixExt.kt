import java.lang.IllegalArgumentException
import kotlin.random.Random

fun <T> createMatrix(height: Int, width: Int, initialValue: T): Matrix<T> =
    MatrixImpl(height, width, initialValue)

fun createRandomRealMatrix(height: Int, width: Int): Matrix<Double> {
    val matrix = MatrixImpl(height, width, 0.0)
    for (i in 0 until height) {
        for (j in 0 until width) {
            val randomNumber = Random.nextDouble(1.0, 5.0)
            matrix[i, j] = randomNumber
        }
    }
    return matrix
}

fun createRandomComplexMatrix(height: Int, width: Int): Matrix<ComplexNumber> {
    val matrix = MatrixImpl(height, width, ComplexNumber(0.0, 0.0))
    for (i in 0 until height) {
        for (j in 0 until width) {
            val randomNumber = Random.nextDouble(1.0, 5.0)
            matrix[i, j] = ComplexNumber(randomNumber, randomNumber)
        }
    }
    return matrix
}

fun <T> transpose(matrix: Matrix<T>): Matrix<T> {
    val width = matrix.width
    val height = matrix.height
    val initialValue = matrix[0, 0]
    if (matrix.width < 1 || matrix.height < 1) return matrix
    val result = createMatrix(height, width, initialValue)
    for (i in 0 until matrix.width) {
        for (j in 0 until matrix.height) {
            result[i, j] = matrix[j, i]
        }
    }
    return result
}

fun Matrix<Double>.plus(matrix: Matrix<Double>): Matrix<Double> {
    val height = matrix.height
    val width = matrix.width
    val result = MatrixImpl(height, width, 0.0)
    if (this.height != height && this.width != width) {
        throw IllegalArgumentException("Matrices are not equals!")
    } else {
        for (i in 0 until height) {
            for (j in 0 until width) {
                result[i, j] = this[i, j] + matrix[i, j]
            }
        }
    }
    return result
}

@JvmName("plusComplex")
fun Matrix<ComplexNumber>.plus(matrix: Matrix<ComplexNumber>): Matrix<ComplexNumber> {
    val height = matrix.height
    val width = matrix.width
    val result = MatrixImpl(height, width, ComplexNumber(0.0, 0.0))
    if (this.height != height && this.width != width) {
        throw IllegalArgumentException("Matrices are not equals!")
    } else {
        for (i in 0 until height) {
            for (j in 0 until width) {
                result[i, j] = this[i, j] + matrix[i, j]
            }
        }
    }
    return result
}

fun Matrix<Double>.multiply(matrix: Matrix<Double>): Matrix<Double> {
    val result = MatrixImpl(this.height, matrix.width, 0.0)
    if (this.width == matrix.height) {
        for (i in 0 until this.height) {
            for (j in 0 until matrix.width) {
                for (k in 0 until this.width) {
                    result[i, j] += this[i, k] * matrix[k, j]
                }
            }
        }
    } else {
        throw IllegalArgumentException("Width A is not equals height B")
    }
    return result
}

@JvmName("multiplyComplex")
fun Matrix<ComplexNumber>.multiply(matrix: Matrix<ComplexNumber>): Matrix<ComplexNumber> {
    val result = MatrixImpl(this.height, matrix.width, ComplexNumber(0.0, 0.0))
    if (this.width == matrix.height) {
        for (i in 0 until this.height) {
            for (j in 0 until matrix.width) {
                for (k in 0 until this.width) {
                    result[i, j] += this[i, k] * matrix[k, j]
                }
            }
        }
    } else {
        throw IllegalArgumentException("Width A is not equals height B")
    }
    return result
}