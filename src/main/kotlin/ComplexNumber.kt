import kotlin.math.*

class ComplexNumber(var re: Double, var im: Double) {

    // Unary operators
    fun exp(x:ComplexNumber):ComplexNumber = ComplexNumber(re, -im)
    fun module():Double = sqrt(re.pow(2.0) + im.pow(2.0))

    // Comparison
    operator fun compareTo(that: ComplexNumber) = this.module().compareTo(that.module())

    fun exp():ComplexNumber  {
        val r: Double = exp(this.re)
        return ComplexNumber(r*cos(this.im),r*sin(this.im))
    }

    operator fun plus(c: ComplexNumber):ComplexNumber = ComplexNumber(re + c.re, im + c.im)
    operator fun minus(c: ComplexNumber):ComplexNumber = ComplexNumber(re-c.re,im-c.im)
    operator fun times(c: ComplexNumber):ComplexNumber =ComplexNumber(re * c.re - im * c.im, im * c.re + re * c.im)
    operator fun times(d: Double):ComplexNumber =ComplexNumber(re * d, im * d )
    operator fun div(c: ComplexNumber):ComplexNumber {
        val d = c.re.pow(2.0) + c.im.pow(2.0)
        return ComplexNumber((re * c.re + im * c.im) / d, (im * c.re - re * c.im) / d)
    }
    operator fun div(c: Double):ComplexNumber  = ComplexNumber(re / c, im / c)

    // String representation
    override fun toString() = this.re.toString() + (if (this.im < 0) "-" + -this.im else "+" + this.im) + "*i"

}