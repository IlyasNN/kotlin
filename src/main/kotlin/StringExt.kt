fun String.getInputWords(): List<String> {
    return this.split(" ", ignoreCase = true).map { it.toLowerCase().trim() }
}

fun String.toCharMap(): Map<Char, Int> {
    val mapOfChar = mutableMapOf<Char, Int>()
    for (char in this) {
        mapOfChar[char] = mapOfChar[char]?.plus(1) ?: 1
    }
    return mapOfChar
}