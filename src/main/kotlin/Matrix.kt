data class Cell(val row: Int, val column: Int)

interface Matrix<T> {
    val height: Int
    val width: Int

    operator fun get(row: Int, column: Int): T
    operator fun get(cell: Cell) = get(cell.row, cell.column)

    operator fun set(row: Int, column: Int, value: T)
    operator fun set(cell: Cell, value: T) = set(cell.row, cell.column, value)
}

data class MatrixImpl<T>(
    override val height: Int,
    override val width: Int,
    private val initialValue: T
) : Matrix<T> {

    private val values = MutableList(height) { MutableList(width) { initialValue } }

    override fun get(row: Int, column: Int): T {
        return values[row][column]
    }

    override fun set(row: Int, column: Int, value: T) {
        values[row][column] = value
    }

    override fun toString(): String {
        var matrixString = ""
        for (i in 0 until height) {
            matrixString += "${values[i]}\n"
        }
        return matrixString
    }
}