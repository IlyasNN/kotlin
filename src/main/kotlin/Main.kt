import kotlinx.coroutines.*
import kotlin.system.measureTimeMillis

fun main() {

    val com1 = ComplexNumber(re = 4.0, im = 20.0)
    val com2 = ComplexNumber(re = 5.0, im = 6.0)
    println(com1 / com2)

    val matrix = createRandomRealMatrix(2, 3)
    val otherMatrix = createRandomRealMatrix(3, 4)
    println(matrix)
    println(otherMatrix)
    println(matrix.multiply(otherMatrix))
    val complexMatrix = createRandomComplexMatrix(3, 3)
    val otherComplexMatrix = createRandomComplexMatrix(3, 3)
    println(complexMatrix)
    println(otherComplexMatrix)
    println(complexMatrix.plus(otherComplexMatrix))
    println("transpose complex: \n${transpose(complexMatrix)}")

    runBlocking {
        val words = getWordsFromFile()
        val longWord = getRandomLongWordFromFile(10)
        println("Случайное слово: $longWord\n" +
                "Вводите слова через пробел :)\n")
        writeWordsToFile(readLine()!!.getInputWords())
        println(getMyWords())
        val myWords = getMyWords()
        val time = measureTimeMillis {
            val score = async { calculateScore(myWords, words, longWord) }
            println("Счет: ${score.await()}")
        }
        println("Потрачено времени на поиск: $time мс")
    }
}

private fun foundWordSymbols(word: String, longWord: String): Int {
    val wordCharMap = word.toCharMap()
    val longWordCharMap = longWord.toCharMap()
    for ((char, count) in wordCharMap) {
        if (longWordCharMap[char] ?: 0 < count) {
            return 0
        }
    }
    return word.length
}

suspend fun calculateScore(
    inputWords: List<String>,
    from: List<String>,
    longWord: String
) = coroutineScope {
    var score = 0
    (inputWords.indices).map { index ->
        async {
            val word = inputWords[index]
            if (from.find { word == it } != null) {
                val symbols = foundWordSymbols(word, longWord)
                score += symbols
            }
        }
    }.awaitAll()
    score
}