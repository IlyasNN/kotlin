import java.io.File
import kotlin.random.Random

private const val PATH = "src/words.txt"
private const val MY_WORDS_PATH = "src/my_words.txt"

fun getWordsFromFile(): List<String> {
    val bufferedReader = File(PATH).bufferedReader()
    return bufferedReader.use { it.readLines() }.map { it.toLowerCase() }
}

fun getRandomLongWordFromFile(length: Int): String {
    val longWords = getWordsFromFile().filter { it.length >= length }
    val randomIndex = Random.nextInt(0, longWords.size)
    return longWords[randomIndex]
}

fun writeWordsToFile(words: List<String>) {
    val file = File(MY_WORDS_PATH)
    file.printWriter().use { out ->
        for (word in words) {
            out.println(word)
        }
    }
}

fun getMyWords(): List<String> {
    val bufferedReader = File(MY_WORDS_PATH).bufferedReader()
    return bufferedReader.use { it.readLines() }.map { it.toLowerCase() }
}