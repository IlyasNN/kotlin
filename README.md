# Ilya Solovyov
# 18pi-1
# Home work Kotlin


### Description
I was on ios development in the second module. So I completed the tasks using the console.
### Core functionality
* Complex number
* Matrix
* Game "WORDS" using coroutines

### Info

The search for words in the file is performed asynchronously.
I also completed tasks gradually, you can look at the commit history

### Contact info
eMail: SolovyovIlya52@yandex.ru
